Demo to build docker images and run container with ansible playbook - Target machine is Amazon Linux Ec2 Instance from AWS
This demo is having the following stuff
 - my-playbook.yaml : this is the ansible playbook
 - hosts : this file contain the target machine information (ip address, ssh key, user)
 - keypair.pem: this is the private key which we will use for ssh to target machine
 - ansible.cfg: this is the configuration file for ansible
 - project-vars: this is the external variable file
 - todoapp: this is the folder which contain the application (from https://docs.docker.com/get-started/02_our_app/)

Steps to run the playbook

Step 1: edit the hosts file with the respective ip address to the target machines

Step 2: enter the private key for ssh to the keypair.pem file

Step 3: edit the respective port (my_port) for the application to start on the target virtual machine in external variable file "project-vars". Note: you have to open the inbound rule on respective security group on AWS console for target virtual machine

Step 4: run the command "ansible-playbook my-playbook.yaml"

You can try to access the application via 2 virtual machine here from my lab. (just need to put more virtual machine in the hosts file and you will have ton of container). Below, I get the my_port: 5000 to start the application
54.179.125.59:5000
13.229.85.245:5000

test again
test second time
add after stage